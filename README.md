Test project for Larnu Spa - Services APIs with Handler (Case : Planets)
=========================================================================

Overview
--------

**Test API services with Django and Handler with React.js**


Goals
-----


As a API services data with Django we are trying to address following goals:

1. Get detailed information of planet by their id 
2. Get list with detailed information about the planets including pagination 
3. Creation of new planets
4. Edit information about planets
5. Delete planets


With the React.js data handler we hope to cover the following goals:

1. Render interface to navigate the options available in the API 
2. Interface for planet listing, with paging obtained from API
3. Form to create and edit planets
4. Option to delete planets

Requirements
------------

1. Python (3.8.x)
2. Django (3.0)
3. Django REST Framework (3.10, 3.11)
4. React.js (17.0.1)

- It is **highly** recommend and only officially support the latest patch release of each React.js, Python, Django and REST Framework series.

Installation APIs Service
-------------------------------------

    # Use pip or pip3 (execute as superuser or administrator)
    $ pip install virtualenv

    # Creating and activate virtualenv with python version 3.8.O
      # example:
        $ virtualenv -p "/usr/bin/python3.8" my_env
        $ source my_env/bin/activate
   
    # Recommended to run in virtual environments
    $ pip install Django
    $ pip install djangorestframework
    $ pip install django-cors-headers


Running the app
---------------
    # It is recommended to create a virtualenv for testing. Assuming it is already installed and activated
    $ git clone https://gitlab.com/lefhiros/larnu_project.git
    $ cd larnu_project

    # run to start the server API Service (Back-End)
    $ python3 manage.py runserver

    # run to start the server (Front-End)
    $ cd frontend
    $ npm install
    $ npm install react-st-modal
    $ npm start



Planet Services API
-------------------

| Method     | URI             | Description                    |
|------------|-----------------|--------------------------------|
| `GET`      | `planets`       | `Get all Planets`              |
| `POST`     | `planets`       | `Create new Planet`            |
| `GET`      | `planets/{id}`  | `Get Planet by ID`             |
| `PATCH`    | `planets/{id}`  | `Edit Planet by ID`            |
| `DELETE`   | `planets/{id}`  | `Delete Planet by ID`          |

Default Browser
---------------
- Browse to http://localhost:8000 (APIs Service)
- Browse to http://localhost:3000 (Front-End)
