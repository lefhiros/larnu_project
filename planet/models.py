from django.db import models

# Planet Model
class Planet(models.Model):
    name         = models.CharField(max_length=300,null=False, blank=False, default='')
    satellites   = models.IntegerField(null=True, blank=True, default=0)
    diameter     = models.DecimalField (decimal_places=4,max_digits=300,null=True, blank=True, default=0.00) # in Kms

    class Meta:
        ordering = ('name',)