from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .models import Planet
from .serializers import *


@api_view(['GET', 'POST'])
def planets_list(request):
    if request.method == 'GET':
        data = []
        nextPage = 0
        previousPage = 1
        planet = Planet.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(planet, 10)

        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = PlanetSerializer(data,context={'request': request} ,many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data , 'count': paginator.count, 'numpages' : paginator.num_pages, 'nextlink': '/planets/?page=' + str(nextPage), 'prevlink': '/planets/?page=' + str(previousPage)})

    elif request.method == 'POST':
        serializer = PlanetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PATCH', 'DELETE'])
def planets_detail(request, pk):
    try:
        planet = Planet.objects.get(pk=pk)
    except Planet.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PlanetSerializer(planet,context={'request': request})
        return Response(serializer.data)

    elif request.method == 'PATCH':
        serializer = PlanetSerializer(planet, data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        planet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)