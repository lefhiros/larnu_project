import React, {Component} from 'react'
import {BrowserRouter} from 'react-router-dom'
import {Route} from 'react-router-dom'
import PlanetsList from './Planets/List'
import PlanetCreateUpdate from './Planets/CreateOrUpdate'
import './App.css'

// The basic layout is established
const BaseLayout = () => (

  // The structure of the navigation
  <div className="container">
    <nav>
      <ul id="menu">
        <li>
          <a className="li-item" href="/">Listado de Planetas</a>
        </li>
        <li>
          <a className="li-item" href="/planet">Crear Planeta</a>
        </li>
      </ul>
    </nav>

    <div className="content">
      <Route path="/" exact component={PlanetsList} />
      <Route path="/planet/:id" component={PlanetCreateUpdate} />
      <Route path="/planet/" exact  component={PlanetCreateUpdate} />
    </div>
  </div>
)

// The component is rendered depending on the path
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <BaseLayout/>
      </BrowserRouter>
    )
  }
}

export default App