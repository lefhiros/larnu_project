import React, {Component, createRef} from 'react'
import { Alert } from 'react-st-modal'
import Services from './Services'

// The class of services is defined
const services = new Services()

// Class for rendering view to create and edit planets
class CreateOrUpdate extends Component {
    constructor(props) {
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.name       = createRef()
        this.satellites = createRef()
        this.diameter   = createRef()
        this.state = {
            value : {
                'title'        : 'Agregar Planeta',
                'title_submit' : 'Guardar',
                'name'         : '',
                'satellites'   : '',
                'diameter'     : ''
            }
        }
    }

    componentDidMount(){
        const {match : {params}} = this.props
        if(params && params.id){
            this.title_submit = 'Actualizar'
            services.getPlanet(params.id).then((c) => {   
                this.setState ({ 
                    value : {
                        'title'         : 'Editar Planeta: ' + c.name,
                        'title_submit'  : 'Actualizar',
                        'name'          : c.name,
                        'satellites'    : c.satellites,
                        'diameter'      : c.diameter
                    }
                })
            })
        }
    }

    handleCreate(){
        services.createPlanet({
            "name"       : this.name.current.value,
            "satellites" : this.satellites.current.value ? this.satellites.current.value : 0,
            "diameter"   : this.diameter.current.value,
        }).then((result)=>
        {
            Alert('El planeta ha sido creado','Exito!', 'Cerrar')
            this.props.history.push('/')
        }).catch(() =>
        {
            Alert('Ha ocurrido un error! Por favor verifique el formulario', 'Error!')
        })
    }

    handleUpdate(id){
        services.updatePlanet({
            "id"         : id,
            "name"       : this.name.current.value,
            "satellites" : this.satellites.current.value ? this.satellites.current.value : 0,
            "diameter"   : this.diameter.current.value,
        }).then((result)=>
        {
            Alert('El planeta ha sido actualizado','Exito!', 'Cerrar')
            this.props.history.push('/')
        }).catch(()=>
        {
            Alert('Ha ocurrido un error! Por favor verifique el formulario', 'Error!')
        })
    }

    handleSubmit(event) {
        const {match: {params}} = this.props

        if(params && params.id){
          this.handleUpdate(params.id)
        }else{
          this.handleCreate()
        }

        event.preventDefault()
    }

    render() {
        const {value} = this.state
        return(
            <div className="planets--add-edit">
                <h2 className="planet-add-edit">{value.title}</h2>
                <form onSubmit={this.handleSubmit}>
                    <table className="table-form">
                        <tbody>
                            <tr className="tr-form">
                                <td>
                                    <label>Nombre:</label>
                                </td>
                                <td>
                                    <input className="form-control" placeholder="Nombre del Planeta"type="text" required ref={this.name} defaultValue={value.name} />
                                </td>
                            </tr>
                            <tr className="tr-form">
                                <td>
                                    <label>Satelites:</label>
                                </td>
                                <td>
                                    <input className="form-control" placeholder="0" required type="number" min="0" ref={this.satellites} defaultValue={value.satellites}/>
                                </td>
                            </tr>
                            <tr className="tr-form">
                                <td>
                                    <label>Diametro:</label>
                                </td>
                                <td>
                                    <input className="form-control" placeholder="0.00"  step="0.0000"  required ref={this.diameter} defaultValue={value.diameter}/>
                                </td>
                            </tr>
                            <tr className="button-group">
                            <td colSpan="2">
                                <button className="button-table" type="submit">{value.title_submit}</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        )
    }
}

export default CreateOrUpdate