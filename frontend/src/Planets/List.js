import React, { Component } from  'react'
import { Confirm } from 'react-st-modal'
import Services  from  './Services'

const services = new Services()

class List extends Component {

    constructor(props) {
        super(props)
        this.state  = {
            planets: [],
            nextPageURL:  '',
            prevPageURL:  '',
            showTable: false
        }
        this.nextPage      =  this.nextPage.bind(this)
        this.previousPage  =  this.previousPage.bind(this)
        this.handleDelete  =  this.handleDelete.bind(this)
    }

    componentDidMount() {
        var self = this
        services.getPlanets().then(function(result) {
            self.setState({
                planets     : result.data, 
                nextPageURL : result.nextlink,
                prevPageURL : result.prevlink,
                showTable   : result.count === 0 ? false : true

            })
        })
    }

    handleDelete(e,id){
        var self = this
        services.deletePlanet({id: id}).then(()=>{
            services.getPlanets().then(function(result) {
                self.setState({
                    planets     : result.data, 
                    nextPageURL : result.nextlink, 
                    prevPageURL : result.prevlink,
                    showTable   : result.count === 0 ? false : true
                })
            })
        })
    }

    nextPage(){
        var self = this
        services.getPlanetsByURL(this.state.nextPageURL).then((result) => {
            self.setState({ 
                planets     : result.data, 
                nextPageURL : result.nextlink, 
                prevPageURL : result.prevlink
            })
        })
    }

    previousPage(){
        var  self  =  this
        services.getPlanetsByURL(this.state.prevPageURL).then((result) => {
            self.setState({ 
                planets     : result.data, 
                nextPageURL : result.nextlink, 
                prevPageURL : result.prevlink
            })
        })
    }
    
    render() {
        return (
            <div className="planets--list">
                <h2 className="planet-list">Listado de Planetas</h2>
                <div className="planet-container" style={{display: this.state.showTable ? 'block' : 'none' }}>
                    <table  className="table">
                        <thead  key="thead">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Satélites</th>
                                <th>Diámetro</th>
                                <th className="action-row">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.planets.map( c  =>
                            <tr key={c.id}>
                                <td>{c.id}  </td>
                                <td>{c.name}</td>
                                <td>{c.satellites}</td>
                                <td>{c.diameter} Kms</td>
                                <td className="action-row">
                                    <a className="button-update" href={"/planet/" + c.id}> Editar</a>
                                    <button  className="button-delete" onClick={ async (e) => {
                                        const result = await Confirm('¿Esta seguro en eliminar el planeta '+c.name+'?', 'Сonfirmación', 'Si', 'No')
                                        if (result) { this.handleDelete(e,c.id) }
                                    }}>Eliminar</button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                    <div className="paginate-control">
                        <button className="button-table" onClick={this.previousPage}>Anterior</button>
                        <button className="button-table" onClick={this.nextPage}>Siguiente</button>
                    </div>
                </div>
                <span className="alert-nothing-planet" style={{display: this.state.showTable ? 'none' : 'block' }}>
                    No se han encontrado planetas
                </span>
            </div>
        )
    }
}
export default List