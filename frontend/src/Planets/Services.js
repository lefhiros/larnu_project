import axios from 'axios'
const API_URL = 'http://localhost:8000'

// Client services for planets API
export default class Services{

    // Get the list of planets
    async getPlanets() {
        const url = `${API_URL}/planets/`
        const response = await axios.get(url)
        return response.data
    }

    // Get the list of planets by Url
    async getPlanetsByURL(link){
        const url = `${API_URL}${link}`
        const response = await axios.get(url)
        return response.data
    }

    // Get planet data by id
    async getPlanet(id) {
        const url = `${API_URL}/planets/${id}`
        const response = await axios.get(url)
        return response.data
    }

    // Delete planet by id
    deletePlanet(planet){
        const url = `${API_URL}/planets/${planet.id}`
        return axios.delete(url)
    }

    // Create new planet data
    createPlanet(planet){
        const url = `${API_URL}/planets/`
        return axios.post(url, planet)
    }

    // Edit planet data by id
    updatePlanet(planet){
        const url = `${API_URL}/planets/${planet.id}`
        return axios.patch(url, planet)
    }
}
 
